#!/usr/bin/env bash
set -euo pipefail

trap exit 2

cp /config/sshd_config /etc/ssh/

service ssh start
/usr/sbin/rsyslogd >/dev/null 2>&1

tail -F /var/log/auth.log 
