# CTFSSH

A simple container that runs SSH server such that

- users can log in without a password, and
- start remote port forwarding, and
- *nothing else*.

Namely, users are not allowed to

- run any shell commands, or
- access the container's local network.

There is only one user `guest` with empty password. There is no risk allowing
anyone to login, as the user cannot do anything useful except giving us access
to their netowrk.

This container may be useful for red-teaming operations as well as for playing
CTFs.

## Getting started

```bash
# download the container
docker pull staticnoise/ctfssh

# start the container
docker run -it -p 3128 -p 2222:22 staticnoise/ctfssh

# connect to the SSH and start a remote port forwarding
ssh -o port=2222 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -N -R 3128 guest@127.0.0.1
```

## Repository

You can find the source code on my [GitLab](https://gitlab.com/staticnoise/ctfssh).

## License

```
Copyright (c) Adam Chovanec

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
```
