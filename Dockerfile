FROM debian:12-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install -y --no-install-recommends openssh-server rsyslog
RUN useradd -m -p '' -s /usr/sbin/nologin guest

COPY ./config/ /config/

EXPOSE 22
EXPOSE 3128

ENTRYPOINT [ "bash", "/config/entrypoint.sh" ]
